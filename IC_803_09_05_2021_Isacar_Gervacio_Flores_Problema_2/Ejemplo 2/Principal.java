import java.io.Console;

public class Principal {
    public static void main(String[] args) {
        System.out.println("==> CalculadoraInt --> resultado = " + Principal.engine_1().calculate(5, 5));
        System.out.println("==> CalculadoraLong --> resultado = " + Principal.engine_2().calculate(6, 2));
        System.out.println("==> CalculadoraInt --> resultado = " + Principal.engine_3().calculate(6, 8));
        System.out.println("==> CalculadoraLong --> resultado = " + Principal.engine_4().calculate(2, 4));
    }

    // Retorna un objeto de tipo "CalculadoraInt"
    private static CalculadoraInt engine_1() {
        return (x, y) -> x * y;
    }

    // Retorna un objeto de tipo "CalculadoraLong"
    private static CalculadoraLong engine_2() {
        return (x, y) -> x - y;
    }

    // Retorna un objeto de tipo "CalculadoraLong"
    private static CalculadoraInt engine_3() {
        return (x, y) -> x + y;
    }

    // Retorna un objeto de tipo "CalculadoraLong"
    private static CalculadoraLong engine_4() {
        return (x, y) -> x / y;
        //posible respuesta a la solucion de la divicion entre 0
        try{
            if (y != 0)
            if (x, != 0)
            throw new ArithmeticException("Exception: divide by zero");
            return x / y;
        }
        catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }

    }

}
